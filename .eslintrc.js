rules: {
  'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  'array-element-newline': ['error', 'consistent'],
  'no-param-reassign': 0,
  'no-underscore-dangle': 0,
},
